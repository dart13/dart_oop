mixin Walk {
  void walk() {
    print('Can walk');
  }
}
abstract class Animal {
  String name;
  int numOfLegs;

  Animal(this.name, this.numOfLegs);

  String getName() {
    return name;
  }

  int getNumOfLegs() {
    return numOfLegs;
  }

  void eat();
  void sleep();
  void speak();
}

abstract class LandAnimal extends Animal {
  LandAnimal(String name, int numOfLegs) : super(name, numOfLegs);
}

abstract class Poulty extends Animal implements Flyable {
  Poulty(String name, int numOfLegs) : super(name, numOfLegs);
}

abstract class Reptile extends Animal {
  Reptile(String name, int numOfLegs) : super(name, numOfLegs);
}

class Dog extends LandAnimal with Walk {
  Dog(String name, int numOfLegs) : super(name, numOfLegs);

  @override
  void eat() {
    print('Dog $name eat');
  }

  @override
  void sleep() {
    print('Dog $name sleep');
  }

  @override
  void speak() {
    print('Dog $name speak : Woof Woof');
  }

  void run() {
    print('Dog $name can run');
  }
}

class Bird extends Poulty with Walk{
  Bird(String name, int numOfLegs) : super(name, numOfLegs = 2);

  @override
  void eat() {
    print('Bird $name eat');
  }

  @override
  void sleep() {
    print('Bird $name sleep');
  }

  @override
  void speak() {
    print('Bird $name speak : tweet tweet');
  }

  @override
  void fly() {
    print('Bird $name fly');
  }
}

class Snake extends Reptile {
  Snake(String name, int numOfLegs) : super(name, numOfLegs = 0);

  @override
  void eat() {
    print('Snake $name eat');
  }

  @override
  void sleep() {
    print('Snake $name sleep');
  }

  @override
  void speak() {
    print('Snake $name speak : hiss hiss');
  }
  
  void crawl() {
    print('Snake $name can crawl');
  }
}

abstract class Vahicle {
  String engine;

  Vahicle(this.engine);

  String getEngine() {
    return engine;
  }

  void startEngine();
  void stopEngine();
}

class Flyable {
  void fly() {}
}

class Plane extends Vahicle implements Flyable {
  Plane(super.engine);

  @override
  void fly() {
    print('Plane can fly');
  }

  @override
  void startEngine() {
    print('Plane start engine');
  }

  @override
  void stopEngine() {
    print('Plane stop engine');
  }
}

void main() {
  Dog dog1 = Dog('Cha', 4);
  dog1.eat();
  dog1.sleep();
  dog1.speak();
  dog1.run();
  dog1.walk();

  Bird bird1 = Bird('Suu', 2);
  bird1.eat();
  bird1.sleep();
  bird1.speak();
  bird1.fly();
  bird1.walk();

  Snake snake1 = Snake('Jo', 0);
  snake1.eat();
  snake1.sleep();
  snake1.speak();
  snake1.crawl();

  Plane plane1 = Plane('engine 1');
  plane1.startEngine();
  plane1.fly();
  plane1.stopEngine();
}
